#!/bin/bash
# generate-deb-package.sh
# Copyright (C) 2018  Rienk de Jong <rienk@rienkdejong.nl>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


if [[ $# -ne 2 ]] || [[ ! -n $FAKEROOTKEY ]] ; then
	echo "Usage: fakeroot $0 PKG_NAME VERSION"
	exit -2
fi

PACKAGE_NAME=$1
VERSION=$2
CONTROL_FILE=install/DEBIAN/control
POSTINST_FILE=install/DEBIAN/postinst
INSTALL_PATH=install/
COPYRIGHT_FILE=install/usr/share/doc/$PACKAGE_NAME/copyright

mkdir -p install/usr/share/doc/$PACKAGE_NAME/

mkdir -p install/DEBIAN

cp -r etc $INSTALL_PATH/etc

chmod -R 755 $INSTALL_PATH

cat <<EOF >$CONTROL_FILE
Package: $PACKAGE_NAME
Version: $VERSION
Architecture: all
Section: kernel
Priority: optional
Maintainer: Rienk de Jong <rienk@rienkdejong.nl>
Description: SOCFPGA kernel helpers
 Kernel installation scripts for Intel SOCFPGAs. Scripts facilitate copying
 the correct device trees and remove conflicting kernel files from /boot if it
 is on a FAT partition.
Depends: device-tree-compiler, u-boot-tools
EOF

cat <<EOF >$POSTINST_FILE
#!/bin/sh
# Postinstall placeholder
set -e

exit 0
EOF

chmod 755 $POSTINST_FILE

cat <<EOF >$COPYRIGHT_FILE
Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2019 Rienk de Jong <rienk@rienkdejong.nl>
License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 /usr/share/common-licenses/GPL-2'.
EOF

chmod 644 $COPYRIGHT_FILE

cd $INSTALL_PATH
find . -type f ! -regex '.*.hg.*' ! -regex '.*?debian-binary.*' ! -regex '.*?DEBIAN.*' -printf '%P ' | xargs md5sum > DEBIAN/md5sums
chmod 644 DEBIAN/md5sums
cd ../


dpkg -b $INSTALL_PATH $PACKAGE_NAME\_$VERSION.deb
rm -rf $INSTALL_PATH
